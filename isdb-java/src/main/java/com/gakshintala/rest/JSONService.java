package com.gakshintala.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.gakshintala.Hero;

@Path("/json/hero")
public class JSONService {

    @GET
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    public Hero getTrackInJSON() {
        return new Hero("Iron Man", "Intelligent");
    }

    @POST
    @Path("/post")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createTrackInJSON(String hero, @QueryParam("user") String user,
                                      @HeaderParam("head") String head) {
        String result = "Hero of the day: " + hero + " From User: " + user + " With Header: " + head;
        return Response.status(Response.Status.CREATED).entity(result).build();
    }
}